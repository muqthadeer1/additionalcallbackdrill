//import the fs module and path module in file
const fs = require("fs");
const path = require("path");

//define the function with two parameters id and callBack
function problem1(id, callback) {
  setTimeout(() => {
    let boardFilePath = path.join(__dirname, "boards_1.json");
    fs.readFile(boardFilePath, (error, boardsDetails) => {
      if (error) {
        callback(error);
      }
      let boards = JSON.parse(boardsDetails);
      let findBoardById = boards.find((eachBoard) => {
        return eachBoard.id == id;
      });
      callback(null, findBoardById);
    });
  }, 2000);
}

//create a module to export the function to other file
module.exports = problem1;
