//import the fs module and path module in file
const fs = require("fs");
const path = require("path");

//define the function with two parameters id and callBack
function problem2(id, callback) {
  setTimeout(() => {
    let listPath = path.join(__dirname, "lists_1.json");
    fs.readFile(listPath, (error, list) => {
      if (error) {
        callback(error);
      }
      let listDetails = JSON.parse(list);
      console.log(listDetails[id]);
    });
  }, 2000);
}

//create the module to export the function to other file
module.exports = problem2;

// problem2();
