//import the fs module and path module in file
const fs = require("fs");
const path = require("path");

function problem3(listId, callback) {
  setTimeout(() => {
    const cardsPath = path.join(__dirname, "cards_1.json");

    fs.readFile(cardsPath, "utf-8", (error, data) => {
      if (error) {
        callback(error);
      }
      const cardsData = JSON.parse(data);
      //  cardsData[listId];

      callback(null, cardsData[listId]);
    });
  }, 2000);
}

//create a module to export the function to other file
module.exports = problem3;
