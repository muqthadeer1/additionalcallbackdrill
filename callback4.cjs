// importing three functions from different files
const fnProblem1 = require("./callback1.cjs");
const fnproblem2 = require("./callback2.cjs");
const fnproblem3 = require("./callback3.cjs");

//import fs and path module
const fs = require("fs");
const path = require("path");

//define the function with callback function as argument
function callback4(callback) {
  setTimeout(() => {
    const boardPath = path.join(__dirname, "boards_1.json");

    fs.readFile(boardPath, "utf-8", (error, data) => {
      // console.log(data);
      const boardData = JSON.parse(data);
      const thanosDetails = boardData.find((each) => {
        if (each.name === "Thanos") {
          return each.id;
        }
      });
      const thanosId = thanosDetails.id;
      // console.log(thanosId);

      fnProblem1(thanosId, (error, boards) => {
        if (error) {
          console.log(error);
        } else if (boards) {
          console.log("board details:", boards);
        } else {
          console.log("Board not Found.");
        }
      });

      fnproblem2(thanosId, (error, data) => {
        if (error) {
          console.log(error);
        } else if (data) {
          console.log("list:", data);
        } else {
          console.log("Id not found.");
        }
      });

      const listPath = path.join(__dirname, "lists_1.json");

      fs.readFile(listPath, "utf-8", (error, data) => {
        if (error) {
          console.log(error);
        }
        const listData = JSON.parse(data);

        const findMindId = listData[thanosId].find((each) => {
          if (each.name === "Mind") {
            return true;
          }
        });
        //  console.log(findMindId.id)
        const listId = findMindId.id;

        fnproblem3(listId, (error, data) => {
          if (error) {
            console.log(error);
          } else if (data) {
            console.log("List Of Cards:", data);
          } else {
            console.log("Invalid Card details.");
          }
        });
      });
    });
  }, 2000);
}

// create module to export the function
module.exports = callback4;
