// import the functions from three different files
const fnProblem1 = require("./callback1.cjs");
const fnproblem2 = require("./callback2.cjs");
const fnproblem3 = require("./callback3.cjs");

//import fs and path module
const fs = require("fs");
const path = require("path");

//define the function with callback as argument
function callBack5(callback) {
  const boardPath = path.join(__dirname, "boards_1.json");

  fs.readFile(boardPath, "utf-8", (error, data) => {
    const boardData = JSON.parse(data);
    const thanosDetails = boardData.find((each) => {
      if (each.name === "Thanos") {
        return each.id;
      }
    });
    const thanosId = thanosDetails.id;
    // console.log(thanosId);

    fnProblem1(thanosId, (error, boards) => {
      if (error) {
        console.log(error);
      } else if (boards) {
        console.log("board details:", boards);
      } else {
        console.log("Board not Found.");
      }
    });

    fnproblem2(thanosId, (error, data) => {
      if (error) {
        console.log(error);
      } else if (data) {
        console.log("list:", data);
      } else {
        console.log("Id not found.");
      }
    });

    const listPath = path.join(__dirname, "lists_1.json");

    fs.readFile(listPath, "utf-8", (error, data) => {
      if (error) {
        console.log(error);
      }
      const listData = JSON.parse(data);
      //    console.log(listData[thanosId]);
      const findIds = listData[thanosId].filter((each) => {
        // console.log(each.name);
        if (each.name === "Space" || each.name === "Mind") {
          return true;
        }
      });
      const mindId = findIds.find((each) => {
        if (each.name === "Mind") {
          return true;
        }
      });
      const spaceId = findIds.find((each) => {
        if (each.name === "Space") {
          return true;
        }
      });

      // call the function
      fnproblem3(mindId.id, (error, data) => {
        if (error) {
          console.log(error);
        } else if (data) {
          console.log("List Of mind:", data);
        } else {
          console.log("Invalid Card details.");
        }
      });

      fnproblem3(spaceId.id, (error, data) => {
        if (error) {
          console.log(error);
        } else if (data) {
          console.log("List Of Space:", data);
        } else {
          console.log("Invalid Card details.");
        }
      });
    });
  });
}

//create a module tp export the function to other files
module.exports = callBack5;
