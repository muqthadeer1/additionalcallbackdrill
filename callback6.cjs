// import the three files from different files
const fnProblem1 = require("./callback1.cjs");
const fnproblem2 = require("./callback2.cjs");
const fnproblem3 = require("./callback3.cjs");

//import the fs and path modules
const fs = require("fs");
const path = require("path");

//define the function with callback function as arguments
function callback6(error) {
  const boardPath = path.join(__dirname, "boards_1.json");

  fs.readFile(boardPath, "utf-8", (error, data) => {
    const boardData = JSON.parse(data);
    const thanosDetails = boardData.find((each) => {
      if (each.name === "Thanos") {
        return each.id;
      }
    });
    const thanosId = thanosDetails.id;
    // console.log(thanosId);

    fnProblem1(thanosId, (error, boards) => {
      if (error) {
        console.log(error);
      } else if (boards) {
        console.log("board details:", boards);
      } else {
        console.log("Board not Found.");
      }
    });

    fnproblem2(thanosId, (error, data) => {
      if (error) {
        console.log(error);
      } else if (data) {
        console.log("list:", data);
      } else {
        console.log("Id not found.");
      }
    });

    const listPath = path.join(__dirname, "lists_1.json");

    fs.readFile(listPath, "utf-8", (error, data) => {
      if (error) {
        console.log(error);
      }

      const listData = JSON.parse(data);
      const ids = [];
      Object.keys(listData).forEach((each) => {
        listData[each].map((eachid) => {
          return ids.push(eachid.id);
        });
      });
      // console.log(ids);

      ids.forEach((each) => {
        // console.log(each);
        fnproblem3(each, (error, data) => {
          if (error) {
            console.log("error", error);
          } else if (data) {
            console.log("List Of Cards:", data);
          } else {
            console.log("Invalid Card details.");
          }
        });
      });
    });
  });
}

//create a module to export the function to other files
module.exports = callback6;
