//import the function from other file
const fnProblem1 = require("../callback1.cjs");

let id = "abc122dc";

//call the function which exported
const problem1 = fnProblem1(id, (error, boards) => {
  if (error) {
    console.log(error);
  } else if (boards) {
    console.log("board details:", boards);
  } else {
    console.log("Board not Found.");
  }
});
