// import the function from other file
const fnproblem2 = require("../callback2.cjs");

let id = "mcu453ed";

// call the the which exported
const findListById = fnproblem2(id, (error, data) => {
  if (error) {
    console.log(error);
  } else if (data) {
    console.log("list:", data);
  } else {
    console.log("Id not found.");
  }
});
