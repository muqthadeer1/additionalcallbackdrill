//import the file from other file
const fnproblem3 = require("../callback3.cjs");

const listId = "qwsa221";

// call the function
const findCardsByListId = fnproblem3(listId, (error, data) => {
  if (error) {
    console.log(error);
  } else if (data) {
    console.log("List Of Cards:", data);
  } else {
    console.log("Invalid Card details.");
  }
});
