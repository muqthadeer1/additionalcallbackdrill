//import the function from other file
const fnCallback5 = require("../callback5.cjs");

//call function with callback function as argument
fnCallback5((error) => {
  if (error) {
    console.log(error);
  }
});
